ifneq ($(KERNELRELEASE),)

export top := $(src)

export CONFIG_VIDEO_IT6510=m

obj-y += drivers/media/i2c/

else

SRC := $(shell pwd)

all:
	$(MAKE) -C $(KERNEL_SRC) M=$(SRC)

modules_install:
	$(MAKE) -C $(KERNEL_SRC) M=$(SRC) modules_install

clean:
	$(MAKE) -C $(KERNEL_SRC) M=$(SRC) clean

endif
