# ite-it6510-driver

This is the out-of-tree kernel driver for ITE IT6510 DP to MIPI CSI-2 decoder.
The driver is provided by ITE and forward ported by MediaTek.

